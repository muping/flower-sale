package com.muping.sso.controller;

import com.muping.sso.common.exception.BusinessException;
import com.muping.sso.common.exception.ResponseResult;
import com.muping.sso.common.redis.RedisHandle;
import com.muping.sso.common.utils.Global;
import com.muping.sso.common.utils.JsonUtils;
import com.muping.sso.dto.input.UserLoginInputDto;
import com.muping.sso.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 19:38 2018/5/3
 * 登陆控制器
 */
@RestController
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    private static Integer loginTime = Global.getInteger("login_time");

    @Resource
    private UserService userService;
    @Resource
    private RedisHandle redisHandle;

    @RequestMapping("/login")
    public ResponseResult login(UserLoginInputDto dto) {
        logger.info("dto={}", JsonUtils.toString(dto));
        userService.login(dto);
        return ResponseResult.generateSuccessMsgResponseResult("登陆成功");
    }

    @RequestMapping("/api/isLogin")
    public ResponseResult isLogin(String token) {
        if (StringUtils.isEmpty(token)) {
            throw new BusinessException(402);
        }
        Object o = redisHandle.get(token);
        if (o == null) {
            throw new BusinessException(402);
        }
        //延长token时间
        redisHandle.setExpireTime(token, loginTime.longValue());
        return ResponseResult.generateResponseResult((String) o);
    }
}
