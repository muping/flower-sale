package com.muping.sso.service;

import com.muping.sso.dto.input.UserLoginInputDto;

/**
 * @author muping
 * @date 17:10 2018/5/3
 */
public interface UserService {

    /**
     * 登陆
     *
     * @param dto dto
     */
    void login(UserLoginInputDto dto);
}
