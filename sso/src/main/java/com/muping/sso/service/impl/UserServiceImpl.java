package com.muping.sso.service.impl;

import com.muping.sso.common.AppContext;
import com.muping.sso.common.exception.BusinessException;
import com.muping.sso.common.redis.RedisHandle;
import com.muping.sso.common.utils.Global;
import com.muping.sso.common.utils.JsonUtils;
import com.muping.sso.dal.dao.UserDao;
import com.muping.sso.dal.entity.User;
import com.muping.sso.dto.input.UserLoginInputDto;
import com.muping.sso.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 17:11 2018/5/3
 */
@Service
@SuppressWarnings("all")
public class UserServiceImpl implements UserService {

    private static Integer loginTime = Global.getInteger("login_time");

    @Resource
    private UserDao userDao;
    @Resource
    private RedisHandle redisHandle;

    @Override
    public void login(UserLoginInputDto dto) {
        if (StringUtils.isEmpty(dto.getPhone()) || StringUtils.isEmpty(dto.getPassword())) {
            throw new BusinessException(10000);
        }
        User user = userDao.queryByPhoneAndPassword(dto.getPhone(), DigestUtils.md5Hex(dto.getPassword()));
        if (user == null) {
            throw new BusinessException(10001);
        }
        //登陆成功
        //设置cookie
        AppContext.setCookie(user.getToken());
        //将token保存到redis
        redisHandle.set(user.getToken(), JsonUtils.toString(user), loginTime.longValue());
    }
}
