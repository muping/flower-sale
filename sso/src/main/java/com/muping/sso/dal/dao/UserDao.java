package com.muping.sso.dal.dao;

import com.muping.sso.common.jdbc.Router;
import com.muping.sso.common.jdbc.RouterEnum;
import com.muping.sso.dal.entity.User;
import com.muping.sso.dal.entity.UserExample;
import com.muping.sso.dal.mapper.UserMapper;
import org.apache.commons.collections.CollectionUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
@SuppressWarnings("all")
public class UserDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.sso.dal.dao.UserDao.";

    @Resource
    private UserMapper userMapper;

    @Router(RouterEnum.SLAVE)
    public User queryByPhoneAndPassword(String phone, String password) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andPhoneEqualTo(phone);
        criteria.andPasswordEqualTo(password);
        List<User> users = userMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(users)) {
            return users.get(0);
        }
        return null;
    }
}
