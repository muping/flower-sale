package com.muping.sso.common;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.muping.sso.common.redis.RedisHandle;
import com.muping.sso.common.utils.Global;
import com.muping.sso.dal.entity.User;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author muping
 * @date 16:19 2018/5/2
 * 应用上下文
 */
@Component
@SuppressWarnings("all")
public class AppContext {

    private static Integer loginTime = Global.getInteger("login_time");

    @Resource
    private RedisHandle redisHandle;

    public User getUser() {
        String token = AppContext.getToken();
        Object field = redisHandle.get(token);
        if (field == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson((String) field, new TypeToken<User>() {
        }.getType());
    }

    public static void setCookie(String value) {
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        Cookie cookie = new Cookie("token", value);
        cookie.setMaxAge(loginTime);
        response.addCookie(cookie);
    }

    public static String getToken() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if ("token".equalsIgnoreCase(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

}
