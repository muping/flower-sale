package com.muping.manager.dto.input.sso;

import lombok.Getter;
import lombok.Setter;

/**
 * @author muping
 * @date 17:02 2018/5/3
 */
@Getter
@Setter
public class UserLoginInputDto {

    private String phone;
    private String password;

}
