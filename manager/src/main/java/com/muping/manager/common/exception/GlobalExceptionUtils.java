package com.muping.manager.common.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * 获取exception.properties的类
 */
public class GlobalExceptionUtils {

    private static Logger logger = LoggerFactory.getLogger(GlobalExceptionUtils.class);

    private static Properties p;

    static {
        try {
            logger.info("load exception.properties");
            p = new Properties();
            InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("exception.properties");
            //解决乱码问题
            p.load(new InputStreamReader(stream, "UTF-8"));
        } catch (Exception e) {
            logger.error("read exception.properties error,error message={}", e);
        }
    }

    public static String get(String key) {
        return p.getProperty(key);
    }
}
