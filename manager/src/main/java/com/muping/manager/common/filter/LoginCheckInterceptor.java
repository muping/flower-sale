package com.muping.manager.common.filter;

import com.muping.security.common.exception.BusinessException;
import com.muping.security.common.redis.AppContext;
import com.muping.security.common.redis.RedisHandle;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author muping
 * @date 17:42 2018/5/2
 * 登陆检查拦截器
 */
public class LoginCheckInterceptor implements HandlerInterceptor {

    @Resource
    private RedisHandle redisHandle;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        //获取cookie中的token
        String token = AppContext.getToken();
        if (StringUtils.isEmpty(token)) {
            throw new BusinessException(10008);
        }
        Object field = redisHandle.getMapField("user-token", token);
        if (field == null) {
            throw new BusinessException(10008);
        }
        //登陆成功
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
