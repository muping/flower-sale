package com.muping.manager.common.jdbc;

/**
 * 该注解用于mysql主从分离
 * 如果需要主从分离，需要在此定义注解
 * 定义规则
 * muping库
 * 主库 master
 * 从库 slave
 * 其他库
 * 主库 表名_master
 * 从库 表名_slave
 */
public enum RouterEnum {
    /**
     * muping主库
     */
    MASTER("master"),
    /**
     * muping从库
     */
    SLAVE("slave"),
    /**
     * security主库
     */
    SECURITY_MASTER("securityMaster"),
    /**
     * security从库
     */
    SECURITY_SLAVE("securitySlave");

    private String des;

    RouterEnum(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
