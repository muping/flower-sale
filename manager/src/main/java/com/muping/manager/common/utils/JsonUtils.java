package com.muping.manager.common.utils;

import com.google.gson.Gson;

/**
 * json工具类
 */
public class JsonUtils {

    private static Gson gson = new Gson();

    public static String toString(Object obj) {
        return gson.toJson(obj);
    }
}
