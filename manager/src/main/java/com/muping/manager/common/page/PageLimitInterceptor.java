package com.muping.manager.common.page;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

/**
 * springmvc 分页拦截器
 * 请求url为query*List的处理分页
 */
public class PageLimitInterceptor implements HandlerInterceptor {

    private static final Logger logger= LoggerFactory.getLogger(PageLimitInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        //清除上下文
        PageContext.clear();
        //匹配query*List
        String pattern = "/query[a-zA-Z]*List";
        String uri = request.getRequestURI();
        String substring = uri.substring(uri.lastIndexOf("/"));
        boolean matches = Pattern.matches(pattern, substring);
        if (matches) {
            String pageSize = request.getParameter(Page.PAGE_SIZE_PARAM);
            String pageNo = request.getParameter(Page.PAGE_NO_PARAM);
            Page page = new Page();
            if (StringUtils.isNotBlank(pageSize) && StringUtils.isNotBlank(pageNo)) {
                page = new Page(Integer.valueOf(pageNo), Integer.valueOf(pageSize));
            }
            PageContext.set(page);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
