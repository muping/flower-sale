package com.muping.manager.common;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.muping.security.common.redis.AppContext;
import com.muping.security.common.redis.RedisHandle;
import com.muping.security.dal.entity.User;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 14:09 2018/5/3
 */
@Component
public class SecurityAppContext {

    @Resource
    private RedisHandle redisHandle;

    public User getUser() {
        String token = AppContext.getToken();
        Object field = redisHandle.getMapField("user-token", token);
        if (field == null) {
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson((String) field, new TypeToken<User>() {
        }.getType());
    }

}
