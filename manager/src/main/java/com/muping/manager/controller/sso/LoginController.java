package com.muping.manager.controller.sso;

import com.muping.manager.common.exception.ResponseResult;
import com.muping.manager.common.utils.JsonUtils;
import com.muping.manager.dto.input.sso.UserLoginInputDto;
import com.muping.manager.service.sso.LoginService;
import com.muping.manager.service.sso.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 17:00 2018/5/3
 */
@RestController
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Resource
    private UserService userService;

    @RequestMapping("/login")
    public ResponseResult login(UserLoginInputDto dto) {
        logger.info("inputDto={}", JsonUtils.toString(dto));
        userService.login(dto);
        return ResponseResult.generateSuccessMsgResponseResult("登陆成功");
    }
}
