package com.muping.manager.service.impl.sso;

import com.muping.manager.common.exception.BusinessException;
import com.muping.manager.common.redis.AppContext;
import com.muping.manager.dal.dao.sso.UserDao;
import com.muping.manager.dal.entity.sso.User;
import com.muping.manager.dto.input.sso.UserLoginInputDto;
import com.muping.manager.service.sso.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 17:11 2018/5/3
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;

    @Override
    public void login(UserLoginInputDto dto) {
        if (StringUtils.isEmpty(dto.getPhone()) || StringUtils.isEmpty(dto.getPassword())) {
            throw new BusinessException(10000);
        }
        User user = userDao.queryByPhoneAndPassword(dto.getPhone(), DigestUtils.md5Hex(dto.getPassword()));
        if (user == null) {
            throw new BusinessException(10001);
        }
        //登陆成功
        //设置cookie
        AppContext.setCookie(user.getToken());
        //将token保存到redis
        redisHandle.addMap("user-token", user.getToken(), JsonUtils.toString(user), loginTime);

    }
}
