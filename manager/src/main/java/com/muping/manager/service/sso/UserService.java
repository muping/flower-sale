package com.muping.manager.service.sso;

import com.muping.manager.dto.input.sso.UserLoginInputDto;

/**
 * @author muping
 * @date 17:10 2018/5/3
 */
public interface UserService {

    /**
     * 登陆
     *
     * @param dto
     */
    void login(UserLoginInputDto dto);
}
