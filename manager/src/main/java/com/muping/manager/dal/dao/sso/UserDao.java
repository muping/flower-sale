package com.muping.manager.dal.dao.sso;

import com.muping.manager.common.jdbc.Router;
import com.muping.manager.common.jdbc.RouterEnum;
import com.muping.manager.dal.entity.sso.User;
import com.muping.manager.dal.entity.sso.UserExample;
import com.muping.manager.dal.mapper.sso.UserMapper;
import org.apache.commons.collections.CollectionUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
@SuppressWarnings("all")
public class UserDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.manager.dal.dao.sso.UserDao.";

    @Resource
    private UserMapper userMapper;

    @Router(RouterEnum.SLAVE)
    public User queryByPhoneAndPassword(String phone, String password) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andPhoneEqualTo(phone);
        criteria.andPasswordEqualTo(password);
        List<User> users = userMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(users)) {
            return users.get(0);
        }
        return null;
    }
}
