package com.muping.security.service;

import com.muping.security.dal.entity.${className};
import com.muping.security.data.condition.Query${className}ListCondition;

import java.util.List;

public interface ${className}Service {

    /**
     * 分页查询
     *
     * @param condition 条件
     * @return 结果
     */
    List<${className}> query${className}List(Query${className}ListCondition condition);

    /**
     * 删除${simpleName}
     *
     * @param ${simpleName}Id id
     */
    void delete(Long ${simpleName}Id);

    /**
     * 添加${simpleName}
     *
     * @param ${simpleName} ${simpleName}
     */
    void add(${className} ${simpleName});

    /**
     * id查询
     */
    ${className} queryById(Long ${simpleName}Id);

    /**
     * 更新${simpleName}
     * @param ${simpleName} ${simpleName}
     */
    void update(${className} ${simpleName});
}
