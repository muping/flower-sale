package com.muping.security.controller;

import com.muping.common.exception.ResponseResult;
import com.muping.common.page.Page;
import com.muping.common.utils.JsonUtils;
import com.muping.security.dal.entity.${className};
import com.muping.security.data.condition.Query${className}ListCondition;
import com.muping.security.dto.input.Query${className}ListInputDto;
import com.muping.security.service.${className}Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * ${simpleName}控制器
 */
@RestController
@RequestMapping("/${simpleName}")
@SuppressWarnings("all")
public class ${className}Controller {

    private static final Logger logger = LoggerFactory.getLogger(${className}Controller.class);

    @Resource
    private ${className}Service ${simpleName}Service;

    /**
     * 查询所有${simpleName}
     *
     * @return 所有${simpleName}
     */
    @RequestMapping("/query${className}List")
    public ResponseResult query${className}List(Query${className}ListInputDto dto) {
        logger.info("inputDto={}", JsonUtils.toString(dto));
        Query${className}ListCondition condition = dto.convertToCondition();
        List<${className}> ${simpleName}s = ${simpleName}Service.query${className}List(condition);
        Page page = Page.generatePageResult(${simpleName}s);
        return ResponseResult.generateLayuiResponseResult(page);
    }

    /**
     * id查询
     *
     * @return ${simpleName}
     */
    @RequestMapping("/queryById")
    public ResponseResult queryById(Long ${simpleName}Id) {
        logger.info("${simpleName}Id={}", ${simpleName}Id);
        return ResponseResult.generateResponseResult(${simpleName}Service.queryById(${simpleName}Id));
    }

    /**
     * 删除${simpleName}
     *
     * @return 删除信息
     */
    @RequestMapping("/delete")
    public ResponseResult delete(Long ${simpleName}Id) {
        logger.info("${simpleName}Id={}", ${simpleName}Id);
        ${simpleName}Service.delete(${simpleName}Id);
        return ResponseResult.generateSuccessMsgResponseResult("删除成功");
    }

    /**
     * 添加或者更新${simpleName}
     *
     * @return 信息
     */
    @RequestMapping("/input")
    public ResponseResult input(${className} ${simpleName}) {
        logger.info("${simpleName}={}", JsonUtils.toString(${simpleName}));
        if (${simpleName}.getId() != null) {
            //update
            ${simpleName}Service.update(${simpleName});
            return ResponseResult.generateSuccessMsgResponseResult("更新成功");
        } else {
            //add
            ${simpleName}Service.add(${simpleName});
            return ResponseResult.generateSuccessMsgResponseResult("添加成功");
        }
    }
}
