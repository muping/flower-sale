package com.muping.security.service.impl;

import com.muping.security.dal.dao.${className}Dao;
import com.muping.security.dal.entity.${className};
import com.muping.security.data.condition.Query${className}ListCondition;
import com.muping.security.service.${className}Service;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class ${className}ServiceImpl implements ${className}Service {

    @Resource
    private ${className}Dao ${simpleName}Dao;

    @Override
    public List<${className}> query${className}List(Query${className}ListCondition condition) {
        return ${simpleName}Dao.query${className}List(condition);
    }

    @Override
    @Transactional
    public void delete(Long ${simpleName}Id) {
        ${simpleName}Dao.delete(${simpleName}Id);
    }

    @Override
    @Transactional
    public void add(${className} ${simpleName}) {
        Date date = new Date();
        ${simpleName}.setCreateBy(2L);
        ${simpleName}.setUpdateBy(2L);
        ${simpleName}.setCreateTime(date);
        ${simpleName}.setUpdateTime(date);
        ${simpleName}Dao.add(${simpleName});
    }

    @Override
    public ${className} queryById(Long ${simpleName}Id) {
        return ${simpleName}Dao.queryById(${simpleName}Id);
    }

    @Override
    @Transactional
    public void update(${className} ${simpleName}) {
        ${simpleName}.setUpdateTime(new Date());
        ${simpleName}.setUpdateBy(3L);
        ${simpleName}Dao.update(${simpleName});
    }
}
