package com.muping.security.dto.input;

import com.muping.security.data.condition.Query${className}ListCondition;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Getter
@Setter
public class Query${className}ListInputDto {

    <#list propertyList as item>
    private String ${item};
    </#list>

    public Query${className}ListCondition convertToCondition() {
        Query${className}ListCondition condition = new Query${className}ListCondition();
        <#list propertyList as item>
        if (StringUtils.isNotBlank(${item})) {
            condition.set${item}(${item});
        }
        </#list>
        return condition;
    }

}
