package com.muping.security.dal.dao;

import com.muping.common.jdbc.Router;
import com.muping.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.${className};
import com.muping.security.dal.entity.${className}Example;
import com.muping.security.dal.mapper.${className}Mapper;
import com.muping.security.data.condition.Query${className}ListCondition;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ${className}Dao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.${className}Dao.";

    @Resource
    private ${className}Mapper ${simpleName}Mapper;

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<${className}> query${className}List(Query${className}ListCondition condition) {
        ${className}Example example = new ${className}Example();
        ${className}Example.Criteria criteria = example.createCriteria();
        <#list propertyList as item>
        if (StringUtils.isNotBlank(condition.get${item}())) {
            criteria.and${item}Like(condition.get${item}() + "%");
        }
        </#list>
        return ${simpleName}Mapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void delete(Long ${simpleName}Id) {
        ${simpleName}Mapper.deleteByPrimaryKey(${simpleName}Id);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void add(${className} ${simpleName}) {
        ${simpleName}Mapper.insertSelective(${simpleName});
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public ${className} queryById(Long ${simpleName}Id) {
        return ${simpleName}Mapper.selectByPrimaryKey(${simpleName}Id);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void update(${className} ${simpleName}) {
        ${simpleName}Mapper.updateByPrimaryKeySelective(${simpleName});
    }
}
