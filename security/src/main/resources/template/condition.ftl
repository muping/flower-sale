package com.muping.security.data.condition;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Query${className}ListCondition {

    <#list propertyList as item>
    private String ${item};
    </#list>

}
