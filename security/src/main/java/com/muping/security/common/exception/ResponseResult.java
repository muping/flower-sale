package com.muping.security.common.exception;

import lombok.Getter;
import lombok.Setter;

/**
 * 封装统一的controller返回对象
 * 如果是业务异常 会有对应的错误码，具体参考exception.properties
 * 如果是其他异常，统一返回500作为错误码
 * 如果操作成功，返回200状态码
 */
@Getter
@Setter
public class ResponseResult {

    /**
     * 成功请求的状态码
     */
    private static final Integer SUCCESS_CODE = 200;

    /**
     * 成功失败的状态码（非业务异常）
     */
    private static final Integer ERROR_CODE = 500;

    /**
     * 需要封装的数据（entity，map，list等等）
     */
    private Object data;

    /**
     * 响应码
     */
    private Integer code;

    /**
     * 响应信息
     */
    private String msg;

    /**
     * 私有化构造方法
     */
    private ResponseResult() {
    }


    /**
     * 用于正常controller接口返回(该方法需设置数据)
     *
     * @param data 数据
     * @return ResponseResult
     */
    public static ResponseResult generateResponseResult(Object data) {
        ResponseResult result = new ResponseResult();
        result.code = SUCCESS_CODE;
        result.data = data;
        return result;
    }

    /**
     * 用于正常controller接口返回(该方法需设置数据和提示信息)
     *
     * @param data 数据
     * @param msg  提示信息
     * @return ResponseResult
     */
    public static ResponseResult generateResponseResult(Object data, String msg) {
        ResponseResult result = new ResponseResult();
        result.code = SUCCESS_CODE;
        result.data = data;
        result.msg = msg;
        return result;
    }

    /**
     * 非业务异常
     *
     * @param msg 提示信息
     * @return ResponseResult
     */
    public static ResponseResult generateErrorMsgResponseResult(String msg) {
        ResponseResult result = new ResponseResult();
        result.code = ERROR_CODE;
        result.msg = msg;
        return result;
    }

    /**
     * 业务异常
     *
     * @param msg 提示信息
     * @return ResponseResult
     */
    public static ResponseResult generateErrorMsgResponseResult(Integer code, String msg) {
        ResponseResult result = new ResponseResult();
        result.code = code;
        result.msg = msg;
        return result;
    }

    /**
     * 用于正常controller接口返回（msg为操作成功信息，比如添加成功，删除成功）
     *
     * @param msg 提示信息
     * @return ResponseResult
     */
    public static ResponseResult generateSuccessMsgResponseResult(String msg) {
        ResponseResult result = new ResponseResult();
        result.code = SUCCESS_CODE;
        result.msg = msg;
        return result;
    }
}
