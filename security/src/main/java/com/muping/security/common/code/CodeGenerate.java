package com.muping.security.common.code;

import com.google.common.collect.Lists;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * 代码生成器
 */
public class CodeGenerate {

    private static Logger logger = LoggerFactory.getLogger(CodeGenerate.class);

    private static Configuration cfg;

    private static final String BASE_PATH_COMMON_PATH_1 = "flower-sale\\common";
    private static final String BASE_PATH_COMMON_PATH_2 = "com\\muping\\common";

    private static String projectPath;

    static {
        try {
            cfg = new Configuration(Configuration.VERSION_2_3_23);
            String basePath = System.getProperty("java.class.path");
            projectPath = new File("").getCanonicalPath();
            projectPath += "\\src\\main";
            String[] split = basePath.split(";");
            for (String s : split) {
                if (s.contains(BASE_PATH_COMMON_PATH_1) || s.contains(BASE_PATH_COMMON_PATH_2)) {
                    basePath = s;
                    break;
                }
            }
            cfg.setDirectoryForTemplateLoading(new File(basePath + "/template"));
        } catch (IOException e) {
            logger.error("exception={}", e);
        }
    }

    public static void execute(Class<?> clazz, String str) {
        CodeInfo codeInfo = new CodeInfo(clazz, str);
        //生成controller文件
        generateController(codeInfo);
        //生成service文件
        generateService(codeInfo);
        //生成serviceImpl文件
        generateServiceImpl(codeInfo);
        //生成dao文件
        generateDao(codeInfo);
        //生成inputDto文件
        generateInputDto(codeInfo);
        //生成condition文件
        generateCondition(codeInfo);
        logger.info("生成成功");
    }

    /**
     * 生成service文件
     *
     * @param codeInfo 数据
     */
    private static void generateService(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\service\\" + codeInfo.getClassName()) + "Service.java";
            Template template = cfg.getTemplate("service.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    /**
     * 生成serviceImpl文件
     *
     * @param codeInfo 数据
     */
    private static void generateServiceImpl(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\service\\impl\\" + codeInfo.getClassName()) + "ServiceImpl.java";
            Template template = cfg.getTemplate("serviceImpl.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    /**
     * 生成dao文件
     *
     * @param codeInfo 数据
     */
    private static void generateDao(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\dal\\dao\\" + codeInfo.getClassName()) + "Dao.java";
            Template template = cfg.getTemplate("dao.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    /**
     * 生成inputDto文件
     *
     * @param codeInfo 数据
     */
    private static void generateInputDto(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\dto\\input\\Query" + codeInfo.getClassName()) + "ListInputDto.java";
            Template template = cfg.getTemplate("inputDto.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    /**
     * 生成condition文件
     *
     * @param codeInfo 数据
     */
    private static void generateCondition(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\data\\condition\\Query" + codeInfo.getClassName()) + "ListCondition.java";
            Template template = cfg.getTemplate("condition.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    /**
     * //生成controller文件
     *
     * @param codeInfo 数据
     */
    private static void generateController(CodeInfo codeInfo) {
        try {
            String target = projectPath + "\\java\\com\\muping" + (codeInfo.getPrefix() + "\\controller\\" + codeInfo.getClassName()) + "Controller.java";
            Template template = cfg.getTemplate("controller.ftl");
            Writer out = new FileWriter(target);
            template.process(codeInfo, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            logger.error("exception={}", e);
        }
    }

    @Getter
    @Setter
    public static class CodeInfo {

        private static Logger logger = LoggerFactory.getLogger(CodeInfo.class);

        /**
         * 首字母小写
         */
        private String simpleName;
        /**
         * 前缀
         */
        private String prefix;
        /**
         * 首字母大写
         */
        private String className;
        /**
         * 属性集合
         */
        private List<String> propertyList;

        CodeInfo(Class<?> clazz, String str) {
            try {
                if (StringUtils.isNotBlank(str)) {
                    prefix = str;
                }
                className = clazz.getSimpleName();
                simpleName = toLowerCaseFirstOne(clazz.getSimpleName());
                BeanInfo beanInfo = Introspector.getBeanInfo(clazz, Object.class);
                PropertyDescriptor[] pd = beanInfo.getPropertyDescriptors();
                propertyList = Lists.newArrayList();
                for (PropertyDescriptor p : pd) {
                    String name = p.getName();
                    propertyList.add(name);
                }
            } catch (IntrospectionException e) {
                logger.error("exception={}", e);
            }
        }
    }

    /**
     * 首字母小写
     *
     * @param s 原字符串
     * @return 修改后的字符串
     */
    private static String toLowerCaseFirstOne(String s) {
        if (Character.isLowerCase(s.charAt(0))) {
            return s;
        } else {
            return (new StringBuilder()).append(Character.toLowerCase(s.charAt(0))).append(s.substring(1)).toString();
        }
    }

}
