package com.muping.security.common.jdbc;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 主从分离aop定义
 */
@Aspect
@Component
public class DynamicRouterDataSourceAspectDefine {

    private static Logger logger = LoggerFactory.getLogger(DynamicRouterDataSourceAspectDefine.class);

    @Pointcut("execution(* com.muping.*.dal.dao..*(..))")
    public void pointCut() {

    }

    /**
     * 在执行方法之前进行拦截
     * point.getArgs() 得到的是传入参数的值
     *
     * @param point 连接点
     */
    @Before("pointCut()")
    public void before(JoinPoint point) {
        //获取方法名
        String name = point.getSignature().getName();
        //获取目标对象
        Object target = point.getTarget();
        //获得函数的参数类型
        Class<?>[] parameterTypes = ((MethodSignature) point.getSignature())
                .getMethod().getParameterTypes();
        try {
            Method method = target.getClass().getMethod(name, parameterTypes);
            if (method.isAnnotationPresent(Router.class)) {
                //获取注解值
                Router router = method.getAnnotation(Router.class);
                RouterEnum routerEnum = router.value();
                if (routerEnum.equals(RouterEnum.MASTER)) {
                    //muping主库
                    DynamicRouterDataSource.DataSourceHolder.put(RouterEnum.MASTER.toString());
                } else if (routerEnum.equals(RouterEnum.SLAVE)) {
                    //muping从库
                    DynamicRouterDataSource.DataSourceHolder.put(RouterEnum.SLAVE.toString());
                } else if (routerEnum.equals(RouterEnum.SECURITY_MASTER)) {
                    //security主库
                    DynamicRouterDataSource.DataSourceHolder.put(RouterEnum.SECURITY_MASTER.toString());
                } else if (routerEnum.equals(RouterEnum.SECURITY_SLAVE)) {
                    //security从库
                    DynamicRouterDataSource.DataSourceHolder.put(RouterEnum.SECURITY_SLAVE.toString());
                }
                logger.info("mysql dynamic router:{}", routerEnum.toString());
            } else {
                logger.info("mysql dynamic router:default router");
            }
        } catch (Exception e) {
            logger.error("mysql dynamic error, error message={}", e);
        }
    }

    @After("pointCut()")
    public void after(JoinPoint joinPoint) {
        logger.info("mysql dynamic router:clear context");
        DynamicRouterDataSource.DataSourceHolder.clear();
    }
}
