package com.muping.security.common.page;

/**
 * 分页上下文，用于存储当前线程的分页信息
 */
public class PageContext {

    private static ThreadLocal<Page> pageThreadLocal = new ThreadLocal<>();

    public static void set(Page page) {
        pageThreadLocal.set(page);
    }

    public static Page get() {
        return pageThreadLocal.get();
    }

    public static void clear() {
        pageThreadLocal.remove();
    }
}
