package com.muping.security.common.jdbc;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

/**
 * 用于主从分离数据源
 */
public class DynamicRouterDataSource extends AbstractRoutingDataSource {

    @Override
    protected Object determineCurrentLookupKey() {
        return DataSourceHolder.context.get();
    }

    public static class DataSourceHolder {

        private static ThreadLocal<String> context = new ThreadLocal<>();

        public static void put(String str) {
            context.set(str);
        }

        public static String get() {
            return context.get();
        }

        public static void clear() {
            context.remove();
        }
    }
}
