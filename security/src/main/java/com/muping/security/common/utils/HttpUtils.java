package com.muping.security.common.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.muping.security.common.exception.ResponseResult;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.Map;

/**
 * @author muping
 * @date 23:44 2018/5/2
 * http工具类
 */
public class HttpUtils {

    /**
     * get请求
     *
     * @param url   请求url
     * @param param 参数
     * @return ResponseResult
     */
    public static ResponseResult get(String url, Map<String, String> param) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        try {
            //创建一个httpClient对象
            httpClient = HttpClients.createDefault();
            //创建一个uri对象
            URIBuilder uriBuilder = new URIBuilder(url);
            if (param != null && param.size() > 0) {
                for (Map.Entry<String, String> entry : param.entrySet()) {
                    uriBuilder.addParameter(entry.getKey(), entry.getValue());
                }
            }
            HttpGet get = new HttpGet(uriBuilder.build());
            //执行请求
            response = httpClient.execute(get);
            //取响应的结果
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String string = EntityUtils.toString(entity, "utf-8");
            if (statusCode == 200) {
                //成功访问
                Gson gson = new Gson();
                return gson.fromJson(string, new TypeToken<ResponseResult>() {
                }.getType());
            }
            return ResponseResult.generateErrorMsgResponseResult("http请求失败");
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.generateErrorMsgResponseResult(e.getMessage());
        } finally {
            //关闭httpClient
            if (response != null) {
                try {
                    response.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (httpClient != null) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
