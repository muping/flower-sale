package com.muping.security.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * 用于加在base.properties里面的内容
 */
public class Global {

    private static Logger logger = LoggerFactory.getLogger(Global.class);

    private static Properties p;

    static {
        try {
            logger.info("load base.properties");
            p = new Properties();
            InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("base.properties");
            //解决乱码问题
            p.load(new InputStreamReader(stream, "UTF-8"));
        } catch (Exception e) {
            logger.error("read base.properties error,error message={}", e);
        }
    }

    public static String getString(String key) {
        return p.getProperty(key);
    }

    public static int getInteger(String key) {
        String property = p.getProperty(key);
        try {
            return Integer.valueOf(property);
        } catch (NumberFormatException e) {
            return -1;
        }
    }
}
