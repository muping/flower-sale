package com.muping.security.dto.output;

import com.muping.security.dal.entity.User;
import com.muping.security.dal.entity.enums.UserKindEnum;
import com.muping.security.dal.entity.enums.UserSexEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author muping
 * @date 17:51 2018/5/1
 */
@Getter
@Setter
public class QueryUserListOutputDto {

    private Long id;
    private String name;
    private String nickName;
    private String token;
    private String phone;
    private String sex;
    private String userKind;
    private String email;
    private String isAdmin;

    public QueryUserListOutputDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.nickName = user.getNickName();
        this.token = user.getToken();
        this.phone = user.getPhone();
        this.sex = UserSexEnum.valueOf(user.getSex()).toString();
        this.userKind = UserKindEnum.valueOf(user.getUserKind()).toString();
        this.email = user.getEmail();
        if (user.getIsAdmin() == 1) {
            this.isAdmin = "是";
        } else {
            this.isAdmin = "否";
        }
    }

}
