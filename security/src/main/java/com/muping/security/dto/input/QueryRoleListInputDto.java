package com.muping.security.dto.input;

import com.muping.security.data.condition.QueryRoleListCondition;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Getter
@Setter
public class QueryRoleListInputDto {

    private String id;
    private String code;
    private String name;

    public QueryRoleListCondition convertToCondition() {
        QueryRoleListCondition condition = new QueryRoleListCondition();
        if (NumberUtils.isDigits(id)) {
            condition.setId(Long.valueOf(id));
        }
        if (StringUtils.isNotBlank(code)) {
            condition.setCode(code);
        }
        if (StringUtils.isNotBlank(name)) {
            condition.setName(name);
        }
        return condition;
    }

}
