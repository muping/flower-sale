package com.muping.security.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddMenuInputDto {

    private Long parentId;
    private String name;
    private String url;

}
