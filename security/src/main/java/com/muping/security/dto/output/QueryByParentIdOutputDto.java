package com.muping.security.dto.output;

import com.muping.security.dal.entity.Menu;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryByParentIdOutputDto {

    private Long id;
    private String name;
    private Boolean isParent;

    public QueryByParentIdOutputDto(Menu menu, Boolean isParent) {
        this.id = menu.getId();
        this.name = menu.getName();
        this.isParent = isParent;
    }
}
