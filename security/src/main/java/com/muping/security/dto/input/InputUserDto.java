package com.muping.security.dto.input;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author muping
 * @date 13:52 2018/5/1
 */
@Getter
@Setter
public class InputUserDto {

    private Long id;
    private String name;
    private String nickName;
    private String sex;
    private String userKind;
    private Integer isAdmin;
    private String email;
    private String phone;
    private List<Long> roleList;
    private List<Long> menuList;

}
