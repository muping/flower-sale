package com.muping.security.dto.input;

import com.muping.security.dal.entity.enums.UserKindEnum;
import com.muping.security.dal.entity.enums.UserSexEnum;
import com.muping.security.data.condition.QueryUserListCondition;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Getter
@Setter
public class QueryUserListInputDto {

    private String id;
    private String name;
    private String nickName;
    private String phone;
    private String sex;
    private String userKind;

    public QueryUserListCondition convertToCondition() {
        QueryUserListCondition condition = new QueryUserListCondition();
        if (NumberUtils.isDigits(id)) {
            condition.setId(Long.valueOf(id));
        }
        if (StringUtils.isNotBlank(name)) {
            condition.setName(name);
        }
        if (StringUtils.isNotBlank(nickName)) {
            condition.setNickName(nickName);
        }
        if (StringUtils.isNotBlank(phone)) {
            condition.setPhone(phone);
        }
        if (StringUtils.isNotBlank(sex)) {
            condition.setSex(UserSexEnum.valueOf(sex).name());
        }
        if (StringUtils.isNotBlank(userKind)) {
            condition.setUserKind(UserKindEnum.valueOf(userKind).name());
        }
        return condition;
    }

}
