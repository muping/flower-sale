package com.muping.security.dto.input;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class InputRoleInputDto {

    private Long id;
    private String code;
    private String name;
    private List<Long> permissionList;

}
