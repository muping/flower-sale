package com.muping.security.dto.output;

import com.muping.security.dal.entity.Permission;
import com.muping.security.dal.entity.Role;
import com.muping.security.dal.entity.RolePermission;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
public class QueryRoleListOutputDto {

    private Long id;
    private String name;
    private String code;
    private List<Long> permissionList;

    public QueryRoleListOutputDto(Role role, List<RolePermission> rolePermissionList) {
        this.id = role.getId();
        this.name = role.getName();
        this.code = role.getCode();
        this.permissionList = rolePermissionList.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
    }

}
