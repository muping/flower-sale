package com.muping.security.dto.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author muping
 * @date 11:39 2018/5/2
 */
@Getter
@Setter
public class UserLoginInputDto {

    private String phone;
    private String password;

}
