package com.muping.security.dto.input;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateMenuInputDto {

    private Long id;
    private String name;
    private String url;
    private Long parentId;

}
