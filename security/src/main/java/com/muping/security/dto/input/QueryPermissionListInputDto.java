package com.muping.security.dto.input;

import com.muping.security.data.condition.QueryPermissionListCondition;
import com.muping.security.data.condition.QueryRoleListCondition;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

@Getter
@Setter
public class QueryPermissionListInputDto {

    private String id;
    private String code;
    private String name;

    public QueryPermissionListCondition convertToCondition() {
        QueryPermissionListCondition condition = new QueryPermissionListCondition();
        if (NumberUtils.isDigits(id)) {
            condition.setId(Long.valueOf(id));
        }
        if (StringUtils.isNotBlank(code)) {
            condition.setCode(code);
        }
        if (StringUtils.isNotBlank(name)) {
            condition.setName(name);
        }
        return condition;
    }

}
