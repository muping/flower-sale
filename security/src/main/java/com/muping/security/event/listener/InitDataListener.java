package com.muping.security.event.listener;

import com.muping.security.common.utils.Global;
import com.muping.security.dal.entity.Menu;
import com.muping.security.dal.entity.User;
import com.muping.security.dal.entity.enums.UserKindEnum;
import com.muping.security.dal.entity.enums.UserSexEnum;
import com.muping.security.dto.input.AddMenuInputDto;
import com.muping.security.service.MenuService;
import com.muping.security.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * @author muping
 * @date 18:13 2018/5/2
 * 用于初始化mysql的部分初始数据
 */
@Component
public class InitDataListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger logger = LoggerFactory.getLogger(InitDataListener.class);

    private static String rootName = Global.getString("fs_root_name");
    private static String phone = Global.getString("fs_default_admin_phone");
    private static String password = Global.getString("fs_default_admin_password");

    @Resource
    private MenuService menuService;

    @Resource
    private UserService userService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        logger.info("初始化数据开始");
        //添加菜单根节点
        List<Menu> menus = menuService.queryByParentId(-1L);
        if (CollectionUtils.isEmpty(menus)) {
            AddMenuInputDto dto = new AddMenuInputDto();
            dto.setParentId(-1L);
            dto.setName(rootName);
            menuService.insert(dto);
        }
        //添加管理员账号和密码
        User user = userService.queryByPhoneAndPassword(phone, DigestUtils.md5Hex(password));
        if (user == null) {
            User u = new User();
            u.setEmail("jihuaib@163.com");
            u.setName("admin");
            u.setNickName("admin");
            u.setPhone(phone);
            u.setSex(UserSexEnum.SECRECY.name());
            u.setUserKind(UserKindEnum.STAFF.name());
            u.setPassword(DigestUtils.md5Hex(password));
            u.setIsAdmin(1);
            //生成token
            String s = UUID.randomUUID().toString();
            String token = s.replaceAll("-", "");
            u.setToken(token);
            userService.insert(u);
        }
        logger.info("初始化数据结束");
    }
}
