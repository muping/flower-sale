package com.muping.security.event.filter;

import com.muping.security.common.exception.BusinessException;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.service.AuthService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author muping
 * @date 17:44 2018/5/2
 * 权限检验拦截器
 */
public class AuthPermissionInterceptor implements HandlerInterceptor {

    private static final Logger logger = LoggerFactory.getLogger(AuthPermissionInterceptor.class);

    @Resource
    private AuthService authService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        String uri = request.getRequestURI();
        //获取cookie中的token
        String token = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if ("token".equalsIgnoreCase(cookie.getName())) {
                    token = cookie.getValue();
                }
            }
        }
        if (StringUtils.isEmpty(token)) {
            logger.info("token={},uri={},鉴权失败", token, uri);
            throw new BusinessException(10008);
        }
        ResponseResult result = authService.auth(token, uri);
        if (result.getCode().equals(200)) {
            logger.info("token={},uri={},鉴权成功", token, uri);
            return true;
        }
        logger.info("token={},uri={},鉴权失败", token, uri);
        throw new BusinessException(10000);
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
