package com.muping.security.data.condition;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryPermissionListCondition {

    private Long id;
    private String code;
    private String name;

}
