package com.muping.security.data.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author muping
 * @date 14:05 2018/5/3
 * 用于存储登陆用户的系统菜单
 */
@Getter
@Setter
public class SystemMenuVo {

    private String id;
    private String name;
    private String url;
    private List<SystemMenuVo> menu;
}
