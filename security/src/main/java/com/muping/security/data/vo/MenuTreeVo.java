package com.muping.security.data.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 封装树节点
 */
@Getter
@Setter
public class MenuTreeVo {

    private Long id;
    private String name;
    private String url;
    private Long parentId;
    private Boolean disabled;
    private List<MenuTreeVo> children;

}
