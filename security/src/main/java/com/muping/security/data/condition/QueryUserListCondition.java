package com.muping.security.data.condition;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QueryUserListCondition {

    private Long id;
    private String name;
    private String nickName;
    private String phone;
    private String sex;
    private String userKind;

}
