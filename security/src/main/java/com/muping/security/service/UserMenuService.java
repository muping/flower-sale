package com.muping.security.service;

import com.muping.security.dal.entity.UserMenu;

import java.util.List;

public interface UserMenuService {

    /**
     * 添加用户角色关联关系
     *
     * @param userMenu rp
     */
    void add(UserMenu userMenu);

    /**
     * 根据userId查询
     *
     * @param userId userId
     * @return 集合
     */
    List<UserMenu> queryByUserId(Long userId);

    /**
     * 根据menuId和userId删除
     *
     * @param userId 用户id
     * @param menuId 角色id
     */
    void deleteByMenuIdAndUserId(Long userId, Long menuId);

    /**
     * 删除菜单关联关系
     *
     * @param userId userId
     */
    void deleteByUserId(Long userId);
}
