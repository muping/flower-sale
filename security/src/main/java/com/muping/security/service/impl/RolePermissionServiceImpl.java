package com.muping.security.service.impl;

import com.muping.security.dal.dao.RolePermissionDao;
import com.muping.security.dal.entity.RolePermission;
import com.muping.security.service.RolePermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RolePermissionServiceImpl implements RolePermissionService {

    @Resource
    private RolePermissionDao permissionServiceDao;

    @Override
    @Transactional(rollbackFor = {})
    public void add(RolePermission rp) {
        permissionServiceDao.add(rp);
    }

    @Override
    public List<RolePermission> queryByRoleId(Long roleId) {
        return permissionServiceDao.queryByRoleId(roleId);
    }

    @Override
    public void deleteByRoleId(Long roleId) {
        permissionServiceDao.deleteByRoleId(roleId);
    }

    @Override
    public void deleteByRoleIdAndPermissionId(Long id, Long permissionId) {
        permissionServiceDao.deleteByRoleIdAndPermissionId(id,permissionId);
    }
}
