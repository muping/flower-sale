package com.muping.security.service.impl;

import com.google.common.collect.Lists;
import com.muping.security.common.AppContext;
import com.muping.security.common.exception.BusinessException;
import com.muping.security.dal.dao.MenuDao;
import com.muping.security.dal.entity.Menu;
import com.muping.security.dal.entity.User;
import com.muping.security.data.vo.MenuTreeVo;
import com.muping.security.data.vo.SystemMenuVo;
import com.muping.security.dto.input.AddMenuInputDto;
import com.muping.security.dto.input.UpdateMenuInputDto;
import com.muping.security.service.MenuService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@SuppressWarnings("all")
public class MenuServiceImpl implements MenuService {

    @Resource
    private MenuDao menuDao;

    @Override
    public List<Menu> queryByParentId(Long parentId) {
        if (parentId == null) {
            parentId = -1L;
        }
        return menuDao.queryByParentId(parentId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void delete(Long menuId) {
        Menu menu = menuDao.queryById(menuId);
        if (menu.getParentId() == -1L) {
            throw new BusinessException(10003);
        }
        //判断是否存在子节点，如果存在，不允许删除
        List<Menu> menuList = menuDao.queryByParentId(menuId);
        if (CollectionUtils.isNotEmpty(menuList)) {
            throw new BusinessException(10002);
        }
        menuDao.delete(menuId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void update(UpdateMenuInputDto dto) {
        //根节点是固定的由系统自动生成 不允许修改里面的任何属性
        if (dto.getParentId() == -1L) {
            throw new BusinessException(10003);
        }
        Menu menu = new Menu();
        menu.setId(dto.getId());
        menu.setName(dto.getName());
        menu.setUrl(dto.getUrl());
        menu.setParentId(dto.getParentId());
        menuDao.update(menu);
    }

    @Override
    public List<MenuTreeVo> queryMenuTree() {
        List<Menu> menus = menuDao.queryByParentId(-1L);
        if (CollectionUtils.isEmpty(menus)) {
            //没有根节点
            return null;
        }
        List<MenuTreeVo> voList = Lists.newArrayList();
        for (Menu menu : menus) {
            MenuTreeVo vo = new MenuTreeVo();
            vo.setId(menu.getId());
            vo.setName(menu.getName());
            vo.setUrl(menu.getUrl());
            vo.setParentId(menu.getParentId());
            if (menu.getParentId() == -1L) {
                vo.setDisabled(true);
            } else {
                vo.setDisabled(false);
            }
            List<MenuTreeVo> menuTreeVoList = queryMenuTreeByParentId(menu.getId());
            vo.setChildren(menuTreeVoList);
            voList.add(vo);
        }
        return voList;
    }

    @Override
    public List<MenuTreeVo> queryMenuTreeByParentId(Long parentId) {
        List<Menu> menuList = menuDao.queryByParentId(parentId);
        List<MenuTreeVo> menuTreeVoList = Lists.newArrayList();
        if (CollectionUtils.isEmpty(menuList)) {
            return menuTreeVoList;
        }
        for (Menu menu : menuList) {
            MenuTreeVo vo = new MenuTreeVo();
            vo.setId(menu.getId());
            vo.setName(menu.getName());
            vo.setUrl(menu.getUrl());
            vo.setChildren(queryMenuTreeByParentId(menu.getId()));
            vo.setParentId(menu.getParentId());
            if (menu.getParentId() == -1L) {
                vo.setDisabled(true);
            } else {
                vo.setDisabled(false);
            }
            menuTreeVoList.add(vo);
        }
        return menuTreeVoList;
    }

    @Override
    public void insert(AddMenuInputDto dto) {
        if (dto.getParentId() == -1L) {
            //判断是否存在根节点
            List<Menu> menus = menuDao.queryByParentId(-1L);
            if (CollectionUtils.isNotEmpty(menus)) {
                throw new BusinessException(10001);
            }
        }
        Menu menu = new Menu();
        menu.setName(dto.getName());
        menu.setUrl(dto.getUrl());
        menu.setParentId(dto.getParentId());
        menuDao.insert(menu);
    }

    @Override
    public List<SystemMenuVo> load() {
        //获取登陆用户
        User user = AppContext.getUser();
        if (user == null) {
            throw new BusinessException(10008);
        }
        //获取根节点的id
        List<Menu> menuList = menuDao.queryByParentId(-1L);
        if (CollectionUtils.isEmpty(menuList)) {
            throw new BusinessException(10009);
        }
        Menu root = menuList.get(0);
        List<SystemMenuVo> voList = Lists.newArrayList();
        if (user.getIsAdmin() == 1) {
            //是管理员 加载所有菜单
            List<Menu> menus = menuDao.queryByParentId(root.getId());
            if (CollectionUtils.isNotEmpty(menus)) {
                for (Menu m : menus) {
                    SystemMenuVo vo = new SystemMenuVo();
                    vo.setId(m.getId() + "");
                    vo.setUrl(m.getUrl());
                    vo.setName(m.getName());
                    //查询子菜单
                    List<Menu> subMenuList = menuDao.queryByParentId(m.getId());
                    List<SystemMenuVo> subVoList = Lists.newArrayList();
                    if (CollectionUtils.isNotEmpty(subMenuList)) {
                        for (Menu subm : subMenuList) {
                            SystemMenuVo subVo = new SystemMenuVo();
                            subVo.setId(subm.getId() + "");
                            subVo.setUrl(subm.getUrl());
                            subVo.setName(subm.getName());
                            subVoList.add(subVo);
                        }
                    }
                    vo.setMenu(subVoList);
                    voList.add(vo);
                }
            }
        } else {
            //获取该用户拥有的parentId=-1的菜单
            List<Menu> menus = menuDao.queryByUserIdAndMenuParentId(user.getId(), root.getId());
            if (CollectionUtils.isNotEmpty(menus)) {
                for (Menu m : menus) {
                    SystemMenuVo vo = new SystemMenuVo();
                    vo.setId(m.getId() + "");
                    vo.setUrl(m.getUrl());
                    vo.setName(m.getName());
                    //查询子菜单
                    List<Menu> subMenuList = menuDao.queryByUserIdAndMenuParentId(user.getId(), m.getId());
                    List<SystemMenuVo> subVoList = Lists.newArrayList();
                    if (CollectionUtils.isNotEmpty(subMenuList)) {
                        for (Menu subm : subMenuList) {
                            SystemMenuVo subVo = new SystemMenuVo();
                            subVo.setId(subm.getId() + "");
                            subVo.setUrl(subm.getUrl());
                            subVo.setName(subm.getName());
                            subVoList.add(subVo);
                        }
                    }
                    vo.setMenu(subVoList);
                    voList.add(vo);
                }
            }
        }
        return voList;
    }
}
