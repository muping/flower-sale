package com.muping.security.service;

import com.muping.security.dal.entity.Role;
import com.muping.security.data.condition.QueryRoleListCondition;
import com.muping.security.dto.input.InputRoleInputDto;

import java.util.List;

public interface RoleService {

    /**
     * 分页查询
     *
     * @param condition 条件
     * @return 结果
     */
    List<Role> queryRoleList(QueryRoleListCondition condition);

    /**
     * 删除role
     *
     * @param roleId id
     */
    void delete(Long roleId);

    /**
     * 添加role
     *
     * @param dto dto
     */
    void add(InputRoleInputDto dto);

    /**
     * id查询
     */
    Role queryById(Long roleId);

    /**
     * 更新role
     *
     * @param dto dto
     */
    void update(InputRoleInputDto dto);

    /**
     * 查询所有角色
     *
     * @return 所有角色
     */
    List<Role> queryAll();

    /**
     * roleIdList查询Role
     *
     * @param roleIdList roleIdList
     * @return 集合
     */
    List<Role> queryByRoleIdList(List<Long> roleIdList);
}
