package com.muping.security.service;

import com.muping.security.dal.entity.Permission;
import com.muping.security.data.condition.QueryPermissionListCondition;

import java.util.List;

public interface PermissionService {

    /**
     * 分页查询
     *
     * @param condition 条件
     * @return 结果
     */
    List<Permission> queryPermissionList(QueryPermissionListCondition condition);

    /**
     * 删除权限
     *
     * @param permissionId id
     */
    void delete(Long permissionId);

    /**
     * 添加权限
     *
     * @param permission 权限
     */
    void add(Permission permission);

    /**
     * id查询
     */
    Permission queryById(Long permissionId);

    /**
     * 更新权限
     *
     * @param permission 权限
     */
    void update(Permission permission);

    /**
     * 查询所有权限
     *
     * @return 所有权限
     */
    List<Permission> queryAll();

    /**
     * 根据idList查询
     *
     * @param roleIdList list
     * @return 结果
     */
    List<Permission> queryByPermissionIdList(List<Long> roleIdList);
}
