package com.muping.security.service;


import com.muping.security.common.exception.ResponseResult;

/**
 * @author muping
 * @date 15:20 2018/5/3
 */
public interface AuthService {

    /**
     * 鉴权服务
     *
     * @param token token
     * @param url   url
     * @return ResponseResult
     */
    ResponseResult auth(String token, String url);

}
