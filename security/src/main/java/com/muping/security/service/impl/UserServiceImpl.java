package com.muping.security.service.impl;

import com.google.common.collect.Lists;
import com.muping.security.common.exception.BusinessException;
import com.muping.security.common.redis.RedisHandle;
import com.muping.security.common.utils.Global;
import com.muping.security.dal.dao.UserDao;
import com.muping.security.dal.entity.*;
import com.muping.security.dal.entity.enums.UserKindEnum;
import com.muping.security.dal.entity.enums.UserSexEnum;
import com.muping.security.data.condition.QueryUserListCondition;
import com.muping.security.dto.input.InputUserDto;
import com.muping.security.service.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Resource
    private UserDao userDao;
    @Resource
    private RedisHandle redisHandle;
    @Resource
    private UserRoleService userRoleService;
    @Resource
    private UserMenuService userMenuService;

    private static Integer loginTime = Global.getInteger("login_time");

    @Override
    public List<User> queryUserList(QueryUserListCondition condition) {
        return userDao.queryUserList(condition);
    }

    @Override
    public void delete(Long userId) {
        //删除用户
        userDao.delete(userId);
        //删除角色关联关系
        userRoleService.deleteByUserId(userId);
        //删除菜单关联关系
        userMenuService.deleteByUserId(userId);
    }

    @Override
    public void add(InputUserDto dto) {
        //校验
        if (StringUtils.isEmpty(dto.getPhone())
                || StringUtils.isEmpty(dto.getEmail())
                || StringUtils.isEmpty(dto.getName())
                || StringUtils.isEmpty(dto.getSex())
                || StringUtils.isEmpty(dto.getUserKind())) {
            throw new BusinessException(10004);
        }
        //增加user
        User user = new User();
        user.setEmail(dto.getEmail());
        user.setName(dto.getName());
        user.setNickName(dto.getNickName());
        user.setPhone(dto.getPhone());
        user.setSex(UserSexEnum.valueOf(dto.getSex()).name());
        user.setUserKind(UserKindEnum.valueOf(dto.getUserKind()).name());
        user.setIsAdmin(dto.getIsAdmin());
        user.setPassword(DigestUtils.md5Hex(dto.getPhone()));
        //生成token
        String s = UUID.randomUUID().toString();
        String token = s.replaceAll("-", "");
        logger.info("token={}", token);
        user.setToken(token);
        userDao.addWithGetId(user);
        //存储角色
        if (CollectionUtils.isNotEmpty(dto.getRoleList())) {
            for (Long roleId : dto.getRoleList()) {
                UserRole userRole = new UserRole();
                userRole.setUserId(user.getId());
                userRole.setRoleId(roleId);
                userRoleService.add(userRole);
            }
        }
        //存储菜单
        if (CollectionUtils.isNotEmpty(dto.getMenuList())) {
            for (Long menuId : dto.getMenuList()) {
                UserMenu userMenu = new UserMenu();
                userMenu.setUserId(user.getId());
                userMenu.setMenuId(menuId);
                userMenuService.add(userMenu);
            }
        }
    }

    @Override
    public User queryById(Long userId) {
        return userDao.queryById(userId);
    }

    @Override
    public User queryByPhoneAndPassword(String phone, String password) {
        return userDao.queryByPhoneAndPassword(phone, password);
    }

    @Override
    public void update(InputUserDto dto) {
        //校验
        if (StringUtils.isEmpty(dto.getPhone())
                || StringUtils.isEmpty(dto.getEmail())
                || StringUtils.isEmpty(dto.getName())
                || StringUtils.isEmpty(dto.getSex())
                || StringUtils.isEmpty(dto.getUserKind())) {
            throw new BusinessException(10004);
        }
        //================================更新user====================================
        User user = new User();
        user.setId(dto.getId());
        user.setEmail(dto.getEmail());
        user.setName(dto.getName());
        user.setNickName(dto.getNickName());
        user.setPhone(dto.getPhone());
        user.setSex(UserSexEnum.valueOf(dto.getSex()).name());
        user.setIsAdmin(dto.getIsAdmin());
        user.setUserKind(UserKindEnum.valueOf(dto.getUserKind()).name());
        user.setPassword(DigestUtils.md5Hex(dto.getPhone()));
        userDao.update(user);
        //================================更新user====================================
        //===============================更新角色关系==================================
        List<UserRole> userRoleList = userRoleService.queryByUserId(dto.getId());
        //获取数据库对应角色的所有角色
        List<Long> oriList = userRoleList.stream().map(UserRole::getRoleId).distinct().collect(Collectors.toList());
        List<Long> updateList = dto.getRoleList();
        //处理空指针异常
        if (CollectionUtils.isEmpty(updateList)) {
            updateList = Lists.newArrayList();
        }
        if (CollectionUtils.isEmpty(oriList)) {
            oriList = Lists.newArrayList();
        }
        //取交集
        Collection intersection = CollectionUtils.intersection(oriList, updateList);
        Collection deleteList = CollectionUtils.disjunction(oriList, intersection);
        Collection addList = CollectionUtils.disjunction(updateList, intersection);
        //处理关联关系
        if (CollectionUtils.isNotEmpty(deleteList)) {
            for (Object o : deleteList) {
                Long roleId = (Long) o;
                userRoleService.deleteByRoleIdAndUserId(user.getId(), roleId);
            }
        }
        if (CollectionUtils.isNotEmpty(addList)) {
            for (Object o : addList) {
                Long roleId = (Long) o;
                UserRole userRole = new UserRole();
                userRole.setUserId(user.getId());
                userRole.setRoleId(roleId);
                userRoleService.add(userRole);
            }
        }
        //===============================更新角色关系==================================
        //===============================更新菜单关系==================================
        List<UserMenu> userMenuList = userMenuService.queryByUserId(dto.getId());
        //获取数据库对应角色的所有角色
        List<Long> oriMenuList = userMenuList.stream().map(UserMenu::getMenuId).distinct().collect(Collectors.toList());
        List<Long> updateMenuList = dto.getMenuList();
        //处理空指针异常
        if (CollectionUtils.isEmpty(updateMenuList)) {
            updateMenuList = Lists.newArrayList();
        }
        if (CollectionUtils.isEmpty(oriMenuList)) {
            oriMenuList = Lists.newArrayList();
        }
        //取交集
        Collection intersectionMenu = CollectionUtils.intersection(oriMenuList, updateMenuList);
        Collection deleteMenuList = CollectionUtils.disjunction(oriMenuList, intersectionMenu);
        Collection addMenuList = CollectionUtils.disjunction(updateMenuList, intersectionMenu);
        //处理关联关系
        if (CollectionUtils.isNotEmpty(deleteMenuList)) {
            for (Object o : deleteMenuList) {
                Long menuId = (Long) o;
                userMenuService.deleteByMenuIdAndUserId(user.getId(), menuId);
            }
        }
        if (CollectionUtils.isNotEmpty(addMenuList)) {
            for (Object o : addMenuList) {
                Long menuId = (Long) o;
                UserMenu userMenu = new UserMenu();
                userMenu.setUserId(user.getId());
                userMenu.setMenuId(menuId);
                userMenuService.add(userMenu);
            }
        }
        //===============================更新菜单关系==================================
    }

    @Override
    public void insert(User u) {
        userDao.add(u);
    }

    @Override
    public User queryByToken(String token) {
        return userDao.queryByToken(token);
    }
}
