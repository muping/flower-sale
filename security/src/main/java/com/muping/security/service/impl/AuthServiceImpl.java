package com.muping.security.service.impl;

import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.muping.security.common.exception.BusinessException;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.common.redis.RedisHandle;
import com.muping.security.common.utils.Global;
import com.muping.security.common.utils.JsonUtils;
import com.muping.security.dal.entity.Permission;
import com.muping.security.dal.entity.RolePermission;
import com.muping.security.dal.entity.User;
import com.muping.security.dal.entity.UserRole;
import com.muping.security.service.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author muping
 * @date 15:21 2018/5/3
 */
@Service
public class AuthServiceImpl implements AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthServiceImpl.class);

    @Resource
    private RedisHandle redisHandle;
    @Resource
    private UserService userService;
    @Resource
    private UserRoleService userRoleService;
    @Resource
    private RolePermissionService rolePermissionService;
    @Resource
    private PermissionService permissionService;

    private static Integer cacheTime = Global.getInteger("permission_cache_time");
    private static String permissionMapKey = Global.getString("redis_permission_map_key");


    @Override
    public ResponseResult auth(String token, String url) {
        logger.info("url={},token={}", url, token);
        ResponseResult responseResult = null;
        if (StringUtils.isEmpty(token) || StringUtils.isEmpty(url)) {
            throw new BusinessException(10007);
        }
        User user = userService.queryByToken(token);
        if (user == null) {
            throw new BusinessException(10010);
        }
        if (user.getIsAdmin() == 1) {
            //是管理员
            responseResult = ResponseResult.generateSuccessMsgResponseResult("鉴权成功");
        } else {
            //获取登陆用户的权限
            Object str = redisHandle.getMapField(permissionMapKey, token);
            if (str == null) {
                //缓存失效,需要从数据库读取
                //保存所有权限到redis
                List<UserRole> userRoleList = userRoleService.queryByUserId(user.getId());
                List<Long> roleIdList = userRoleList.stream().map(UserRole::getRoleId).distinct().collect(Collectors.toList());
                Set<String> urlList = Sets.newHashSet();
                for (Long aLong : roleIdList) {
                    List<RolePermission> permissionList = rolePermissionService.queryByRoleId(aLong);
                    List<Long> pIdList = permissionList.stream().map(RolePermission::getPermissionId).distinct().collect(Collectors.toList());
                    List<Permission> permissions = permissionService.queryByPermissionIdList(pIdList);
                    urlList.addAll(permissions.stream().map(Permission::getUrl).distinct().collect(Collectors.toSet()));
                }
                if (urlList.contains(url)) {
                    responseResult = ResponseResult.generateSuccessMsgResponseResult("鉴权成功");
                } else {
                    throw new BusinessException(401);
                }
                redisHandle.addMap(permissionMapKey, user.getToken(), JsonUtils.toString(urlList), cacheTime);
            } else {
                String json = (String) str;
                Gson gson = new Gson();
                List<String> urlList = gson.fromJson(json, new TypeToken<List<String>>() {
                }.getType());
                if (urlList.contains(url)) {
                    responseResult = ResponseResult.generateSuccessMsgResponseResult("鉴权成功");
                } else {
                    throw new BusinessException(401);
                }
            }
        }
        return responseResult;
    }
}
