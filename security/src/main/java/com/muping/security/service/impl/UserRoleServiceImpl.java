package com.muping.security.service.impl;

import com.muping.security.dal.dao.UserRoleDao;
import com.muping.security.dal.entity.UserRole;
import com.muping.security.service.UserRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author muping
 * @date 17:23 2018/5/1
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Resource
    private UserRoleDao userRoleDao;

    @Override
    public void add(UserRole userRole) {
        userRoleDao.insert(userRole);
    }

    @Override
    public List<UserRole> queryByUserId(Long userId) {
        return userRoleDao.queryByUserId(userId);
    }

    @Override
    public void deleteByRoleIdAndUserId(Long userId, Long roleId) {
        userRoleDao.deleteByRoleIdAndUserId(userId,roleId);
    }

    @Override
    public void deleteByUserId(Long userId) {
        userRoleDao.deleteByUserId(userId);
    }
}
