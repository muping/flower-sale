package com.muping.security.service;

import com.muping.security.dal.entity.RolePermission;

import java.util.List;

public interface RolePermissionService {

    /**
     * 添加角色权限关联关系
     *
     * @param rp rp
     */
    void add(RolePermission rp);

    /**
     * 根据roleId查询
     *
     * @param roleId roleId
     * @return 集合
     */
    List<RolePermission> queryByRoleId(Long roleId);

    /**
     * 删除关联关系
     *
     * @param roleId id
     */
    void deleteByRoleId(Long roleId);

    /**
     * 根据roleId和permissionId删除
     *
     * @param id           角色id
     * @param permissionId 权限id
     */
    void deleteByRoleIdAndPermissionId(Long id, Long permissionId);
}
