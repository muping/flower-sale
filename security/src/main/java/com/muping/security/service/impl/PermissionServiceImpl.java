package com.muping.security.service.impl;

import com.muping.security.dal.dao.PermissionDao;
import com.muping.security.dal.entity.Permission;
import com.muping.security.data.condition.QueryPermissionListCondition;
import com.muping.security.service.PermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Resource
    private PermissionDao permissionDao;

    @Override
    public List<Permission> queryPermissionList(QueryPermissionListCondition condition) {
        return permissionDao.queryPermissionList(condition);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void delete(Long permissionId) {
        permissionDao.delete(permissionId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void add(Permission permission) {
        permissionDao.add(permission);
    }

    @Override
    public Permission queryById(Long permissionId) {
        return permissionDao.queryById(permissionId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void update(Permission permission) {
        permissionDao.update(permission);
    }

    @Override
    public List<Permission> queryAll() {
        return permissionDao.queryAll();
    }

    @Override
    public List<Permission> queryByPermissionIdList(List<Long> roleIdList) {
        return permissionDao.queryByPermissionIdList(roleIdList);
    }
}
