package com.muping.security.service;

import com.muping.security.dal.entity.UserRole;

import java.util.List;

public interface UserRoleService {

    /**
     * 添加用户角色关联关系
     *
     * @param userRole rp
     */
    void add(UserRole userRole);

    /**
     * 根据userId查询
     *
     * @param userId userId
     * @return 集合
     */
    List<UserRole> queryByUserId(Long userId);

    /**
     * 根据roleId和userId删除
     *
     * @param userId 用户id
     * @param roleId 角色id
     */
    void deleteByRoleIdAndUserId(Long userId, Long roleId);

    /**
     * 删除角色关联关系
     * @param userId userId
     */
    void deleteByUserId(Long userId);
}
