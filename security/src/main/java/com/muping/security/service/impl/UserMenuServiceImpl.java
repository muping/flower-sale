package com.muping.security.service.impl;

import com.muping.security.dal.dao.UserMenuDao;
import com.muping.security.dal.entity.UserMenu;
import com.muping.security.service.UserMenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author muping
 * @date 17:29 2018/5/1
 */
@Service
public class UserMenuServiceImpl implements UserMenuService {

    @Resource
    private UserMenuDao userMenuDao;

    @Override
    public void add(UserMenu userMenu) {
        userMenuDao.insert(userMenu);
    }

    @Override
    public List<UserMenu> queryByUserId(Long userId) {
        return userMenuDao.queryByUserId(userId);
    }

    @Override
    public void deleteByMenuIdAndUserId(Long userId, Long menuId) {
        userMenuDao.deleteByMenuIdAndUserId(userId, menuId);
    }

    @Override
    public void deleteByUserId(Long userId) {
        userMenuDao.deleteByUserId(userId);
    }
}
