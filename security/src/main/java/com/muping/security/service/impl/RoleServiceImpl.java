package com.muping.security.service.impl;

import com.google.common.collect.Lists;
import com.muping.security.dal.dao.RoleDao;
import com.muping.security.dal.entity.Role;
import com.muping.security.dal.entity.RolePermission;
import com.muping.security.data.condition.QueryRoleListCondition;
import com.muping.security.dto.input.InputRoleInputDto;
import com.muping.security.service.RolePermissionService;
import com.muping.security.service.RoleService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleDao roleDao;
    @Resource
    private RolePermissionService rolePermissionService;

    @Override
    public List<Role> queryRoleList(QueryRoleListCondition condition) {
        return roleDao.queryRoleList(condition);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void delete(Long roleId) {
        roleDao.delete(roleId);
        //删除关联关系
        rolePermissionService.deleteByRoleId(roleId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void add(InputRoleInputDto dto) {
        Role role = new Role();
        role.setCode(dto.getCode());
        role.setName(dto.getName());
        roleDao.insertGetId(role);

        if (CollectionUtils.isNotEmpty(dto.getPermissionList())) {
            //设置关联关系
            for (Long pId : dto.getPermissionList()) {
                RolePermission rp = new RolePermission();
                rp.setRoleId(role.getId());
                rp.setPermissionId(pId);
                rolePermissionService.add(rp);
            }
        }
    }

    @Override
    public Role queryById(Long roleId) {
        return roleDao.queryById(roleId);
    }

    @Override
    @Transactional(rollbackFor = {})
    public void update(InputRoleInputDto dto) {
        Role role = new Role();
        role.setId(dto.getId());
        role.setCode(dto.getCode());
        role.setName(dto.getName());

        List<RolePermission> rolePermissionList = rolePermissionService.queryByRoleId(dto.getId());
        //获取数据库对应角色的所有权限
        List<Long> oriList = rolePermissionList.stream().map(RolePermission::getPermissionId).distinct().collect(Collectors.toList());
        List<Long> updateList = dto.getPermissionList();
        //处理空指针异常
        if (CollectionUtils.isEmpty(updateList)) {
            updateList = Lists.newArrayList();
        }
        if (CollectionUtils.isEmpty(oriList)) {
            oriList = Lists.newArrayList();
        }
        //取交集
        Collection intersection = CollectionUtils.intersection(oriList, updateList);
        Collection deleteList = CollectionUtils.disjunction(oriList, intersection);
        Collection addList = CollectionUtils.disjunction(updateList, intersection);
        //处理关联关系
        if (CollectionUtils.isNotEmpty(deleteList)) {
            for (Object o : deleteList) {
                Long permissionId = (Long) o;
                rolePermissionService.deleteByRoleIdAndPermissionId(role.getId(), permissionId);
            }
        }
        if (CollectionUtils.isNotEmpty(addList)) {
            for (Object o : addList) {
                Long permissionId = (Long) o;
                RolePermission rolePermission = new RolePermission();
                rolePermission.setPermissionId(permissionId);
                rolePermission.setRoleId(role.getId());
                rolePermissionService.add(rolePermission);
            }
        }
        roleDao.update(role);
    }

    @Override
    public List<Role> queryAll() {
        return roleDao.queryAll();
    }

    @Override
    public List<Role> queryByRoleIdList(List<Long> roleIdList) {
        return roleDao.queryByRoleIdList(roleIdList);
    }
}
