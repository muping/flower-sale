package com.muping.security.service;

import com.muping.security.dal.entity.Menu;
import com.muping.security.dal.entity.User;
import com.muping.security.data.vo.MenuTreeVo;
import com.muping.security.data.vo.SystemMenuVo;
import com.muping.security.dto.input.AddMenuInputDto;
import com.muping.security.dto.input.UpdateMenuInputDto;

import java.util.List;

public interface MenuService {

    /**
     * 根据parentId查询子节点
     *
     * @param parentId parentId
     * @return menu集合
     */
    List<Menu> queryByParentId(Long parentId);

    /**
     * id删除
     *
     * @param menuId id
     */
    void delete(Long menuId);

    /**
     * 修改
     *
     * @param dto menu
     */
    void update(UpdateMenuInputDto dto);

    /**
     * 获取整棵树
     *
     * @return MenuTreeVo
     */
    List<MenuTreeVo> queryMenuTree();

    /**
     * 递归获取整棵树
     *
     * @param parentId parentId
     * @return MenuTreeVo
     */
    List<MenuTreeVo> queryMenuTreeByParentId(Long parentId);

    /**
     * 添加菜单
     *
     * @param dto dto
     */
    void insert(AddMenuInputDto dto);

    /**
     * 加载用户菜单
     *
     * @return SystemMenuVo
     */
    List<SystemMenuVo> load();
}
