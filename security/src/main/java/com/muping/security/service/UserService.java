package com.muping.security.service;

import com.muping.security.dal.entity.Menu;
import com.muping.security.dal.entity.User;
import com.muping.security.data.condition.QueryUserListCondition;
import com.muping.security.dto.input.InputUserDto;
import com.muping.security.dto.input.UserLoginInputDto;

import java.util.List;

public interface UserService {

    /**
     * 分页查询
     *
     * @param condition 条件
     * @return 结果
     */
    List<User> queryUserList(QueryUserListCondition condition);

    /**
     * 删除user
     *
     * @param userId id
     */
    void delete(Long userId);

    /**
     * 添加user
     *
     * @param dto dto
     */
    void add(InputUserDto dto);

    /**
     * id查询
     */
    User queryById(Long userId);

    /**
     * 更新user
     *
     * @param dto dto
     */
    void update(InputUserDto dto);

    /**
     * 更新user
     *
     * @param phone dto
     */
    User queryByPhoneAndPassword(String phone, String password);

    /**
     * 添加用户
     *
     * @param u 用户
     */
    void insert(User u);

    /**
     * 根据token查询
     *
     * @param token token
     * @return User
     */
    User queryByToken(String token);
}
