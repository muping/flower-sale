package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.Permission;
import com.muping.security.dal.entity.PermissionExample;
import com.muping.security.dal.mapper.PermissionMapper;
import com.muping.security.data.condition.QueryPermissionListCondition;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class PermissionDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.PermissionDao.";

    @Resource
    private PermissionMapper permissionMapper;

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Permission> queryPermissionList(QueryPermissionListCondition condition) {
        PermissionExample example = new PermissionExample();
        PermissionExample.Criteria criteria = example.createCriteria();
        if (condition.getId() != null) {
            criteria.andIdEqualTo(condition.getId());
        }
        if (StringUtils.isNotBlank(condition.getCode())) {
            criteria.andCodeLike(condition.getCode() + "%");
        }
        if (StringUtils.isNotBlank(condition.getName())) {
            criteria.andNameLike(condition.getName() + "%");
        }
        return permissionMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void delete(Long permissionId) {
        permissionMapper.deleteByPrimaryKey(permissionId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void add(Permission permission) {
        permissionMapper.insertSelective(permission);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public Permission queryById(Long permissionId) {
        return permissionMapper.selectByPrimaryKey(permissionId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void update(Permission permission) {
        permissionMapper.updateByPrimaryKeySelective(permission);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Permission> queryAll() {
        PermissionExample example = new PermissionExample();
        return permissionMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Permission> queryByPermissionIdList(List<Long> roleIdList) {
        PermissionExample example = new PermissionExample();
        PermissionExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(roleIdList);
        return permissionMapper.selectByExample(example);
    }
}
