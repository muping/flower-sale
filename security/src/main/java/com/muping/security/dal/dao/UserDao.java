package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.User;
import com.muping.security.dal.entity.UserExample;
import com.muping.security.dal.mapper.UserMapper;
import com.muping.security.data.condition.QueryUserListCondition;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class UserDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.UserDao.";

    @Resource
    private UserMapper userMapper;

    @Router(RouterEnum.SLAVE)
    public List<User> queryUserList(QueryUserListCondition condition) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        if (condition.getId() != null) {
            criteria.andIdEqualTo(condition.getId());
        }
        if (StringUtils.isNotBlank(condition.getName())) {
            criteria.andNameLike(condition.getName() + "%");
        }
        if (StringUtils.isNotBlank(condition.getNickName())) {
            criteria.andNickNameLike(condition.getNickName() + "%");
        }
        if (StringUtils.isNotBlank(condition.getPhone())) {
            criteria.andPhoneLike(condition.getPhone() + "%");
        }
        if (StringUtils.isNotBlank(condition.getSex())) {
            criteria.andSexEqualTo(condition.getSex());
        }
        if (StringUtils.isNotBlank(condition.getUserKind())) {
            criteria.andUserKindEqualTo(condition.getUserKind());
        }
        return userMapper.selectByExample(example);
    }

    @Router(RouterEnum.MASTER)
    public void delete(Long userId) {
        userMapper.deleteByPrimaryKey(userId);
    }

    @Router(RouterEnum.MASTER)
    public void add(User user) {
        userMapper.insertSelective(user);
    }

    @Router(RouterEnum.SLAVE)
    public User queryById(Long userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    @Router(RouterEnum.MASTER)
    public void update(User user) {
        userMapper.updateByPrimaryKeySelective(user);
    }

    @Router(RouterEnum.MASTER)
    public void addWithGetId(User user) {
        sqlSessionTemplate.insert(BASE_NAMESPACE + "addWithGetId", user);
    }

    @Router(RouterEnum.SLAVE)
    public User queryByPhoneAndPassword(String phone, String password) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andPhoneEqualTo(phone);
        criteria.andPasswordEqualTo(password);
        List<User> users = userMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(users)) {
            return users.get(0);
        }
        return null;
    }

    @Router(RouterEnum.SLAVE)
    public User queryByToken(String token) {
        UserExample example = new UserExample();
        UserExample.Criteria criteria = example.createCriteria();
        criteria.andTokenEqualTo(token);
        List<User> users = userMapper.selectByExample(example);
        if (CollectionUtils.isNotEmpty(users)) {
            return users.get(0);
        }
        return null;
    }
}
