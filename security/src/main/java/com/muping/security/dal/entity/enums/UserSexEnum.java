package com.muping.security.dal.entity.enums;

/**
 * @author muping
 * @date 10:00 2018/4/28
 * 用户性别枚举
 */
public enum UserSexEnum {

    MAN("男"), WOMAN("女"), SECRECY("保密");

    private String des;

    UserSexEnum(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
