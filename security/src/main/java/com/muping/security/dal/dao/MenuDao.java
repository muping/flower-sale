package com.muping.security.dal.dao;

import com.google.common.collect.Maps;
import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.Menu;
import com.muping.security.dal.entity.MenuExample;
import com.muping.security.dal.mapper.MenuMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class MenuDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;
    @Resource
    private MenuMapper menuMapper;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.MenuDao.";

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Menu> queryByParentId(Long parentId) {
        MenuExample example = new MenuExample();
        MenuExample.Criteria criteria = example.createCriteria();
        criteria.andParentIdEqualTo(parentId);
        return menuMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void delete(Long menuId) {
        menuMapper.deleteByPrimaryKey(menuId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void update(Menu menu) {
        menuMapper.updateByPrimaryKeySelective(menu);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public Menu queryById(Long menuId) {
        return menuMapper.selectByPrimaryKey(menuId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void insert(Menu menu) {
        menuMapper.insertSelective(menu);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Menu> queryByUserIdAndMenuParentId(Long userId, long parentId) {
        Map<String, Object> map = Maps.newHashMap();
        map.put("userId", userId);
        map.put("parentId", parentId);
        return sqlSessionTemplate.selectList(BASE_NAMESPACE + "queryByUserIdAndMenuParentId", map);
    }
}
