package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.RolePermission;
import com.muping.security.dal.entity.RolePermissionExample;
import com.muping.security.dal.mapper.RolePermissionMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class RolePermissionDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.RolePermissionDao.";

    @Resource
    private RolePermissionMapper rolePermissionMapper;

    @Router(RouterEnum.SECURITY_MASTER)
    public void add(RolePermission rp) {
        rolePermissionMapper.insert(rp);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<RolePermission> queryByRoleId(Long roleId) {
        return sqlSessionTemplate.selectList(BASE_NAMESPACE + "queryByRoleId", roleId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByRoleId(Long roleId) {
        sqlSessionTemplate.update(BASE_NAMESPACE + "deleteByRoleId", roleId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByRoleIdAndPermissionId(Long id, Long permissionId) {
        RolePermissionExample example = new RolePermissionExample();
        RolePermissionExample.Criteria criteria = example.createCriteria();
        criteria.andRoleIdEqualTo(id);
        criteria.andPermissionIdEqualTo(permissionId);
        rolePermissionMapper.deleteByExample(example);
    }
}
