package com.muping.security.dal.entity.enums;

/**
 * @author muping
 * @date 10:04 2018/4/28
 * 用户类别枚举
 */
public enum UserKindEnum {

    USER("用户"), STAFF("员工");

    private String des;

    UserKindEnum(String des) {
        this.des = des;
    }

    @Override
    public String toString() {
        return this.des;
    }
}
