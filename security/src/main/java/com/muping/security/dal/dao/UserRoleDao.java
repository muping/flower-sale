package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.UserRole;
import com.muping.security.dal.entity.UserRoleExample;
import com.muping.security.dal.mapper.UserRoleMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author muping
 * @date 17:31 2018/5/1
 */
@Repository
public class UserRoleDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.UserRoleDao.";

    @Resource
    private UserRoleMapper userRoleMapper;


    @Router(RouterEnum.SECURITY_MASTER)
    public void insert(UserRole userRole) {
        userRoleMapper.insert(userRole);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<UserRole> queryByUserId(Long userId) {
        UserRoleExample example = new UserRoleExample();
        UserRoleExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        return userRoleMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByRoleIdAndUserId(Long userId, Long roleId) {
        UserRoleExample example = new UserRoleExample();
        UserRoleExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andRoleIdEqualTo(roleId);
        userRoleMapper.deleteByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByUserId(Long userId) {
        UserRoleExample example = new UserRoleExample();
        UserRoleExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        userRoleMapper.deleteByExample(example);
    }
}
