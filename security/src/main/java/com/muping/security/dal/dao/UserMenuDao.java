package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.UserMenu;
import com.muping.security.dal.entity.UserMenuExample;
import com.muping.security.dal.mapper.UserMenuMapper;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author muping
 * @date 17:31 2018/5/1
 */
@Repository
public class UserMenuDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.UserMenuDao.";

    @Resource
    private UserMenuMapper userMenuMapper;


    @Router(RouterEnum.SECURITY_MASTER)
    public void insert(UserMenu userMenu) {
        userMenuMapper.insert(userMenu);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<UserMenu> queryByUserId(Long userId) {
        UserMenuExample example = new UserMenuExample();
        UserMenuExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        return userMenuMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByMenuIdAndUserId(Long userId, Long menuId) {
        UserMenuExample example = new UserMenuExample();
        UserMenuExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andMenuIdEqualTo(menuId);
        userMenuMapper.deleteByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void deleteByUserId(Long userId) {
        UserMenuExample example = new UserMenuExample();
        UserMenuExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        userMenuMapper.deleteByExample(example);
    }
}
