package com.muping.security.dal.dao;

import com.muping.security.common.jdbc.Router;
import com.muping.security.common.jdbc.RouterEnum;
import com.muping.security.dal.entity.Role;
import com.muping.security.dal.entity.RoleExample;
import com.muping.security.dal.mapper.RoleMapper;
import com.muping.security.data.condition.QueryRoleListCondition;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class RoleDao {

    @Resource
    private SqlSessionTemplate sqlSessionTemplate;

    private static final String BASE_NAMESPACE = "com.muping.security.dal.dao.RoleDao.";

    @Resource
    private RoleMapper roleMapper;

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Role> queryRoleList(QueryRoleListCondition condition) {
        RoleExample example = new RoleExample();
        RoleExample.Criteria criteria = example.createCriteria();
        if (condition.getId() != null) {
            criteria.andIdEqualTo(condition.getId());
        }
        if (StringUtils.isNotBlank(condition.getCode())) {
            criteria.andCodeLike(condition.getCode() + "%");
        }
        if (StringUtils.isNotBlank(condition.getName())) {
            criteria.andNameLike(condition.getName() + "%");
        }
        return roleMapper.selectByExample(example);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void delete(Long roleId) {
        roleMapper.deleteByPrimaryKey(roleId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void add(Role role) {
        roleMapper.insertSelective(role);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public Role queryById(Long roleId) {
        return roleMapper.selectByPrimaryKey(roleId);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void update(Role role) {
        roleMapper.updateByPrimaryKeySelective(role);
    }

    @Router(RouterEnum.SECURITY_MASTER)
    public void insertGetId(Role role) {
        sqlSessionTemplate.insert(BASE_NAMESPACE + "insertGetId", role);
    }

    public List<Role> queryAll() {
        RoleExample roleExample = new RoleExample();
        return roleMapper.selectByExample(roleExample);
    }

    @Router(RouterEnum.SECURITY_SLAVE)
    public List<Role> queryByRoleIdList(List<Long> roleIdList) {
        RoleExample example = new RoleExample();
        RoleExample.Criteria criteria = example.createCriteria();
        criteria.andIdIn(roleIdList);
        return roleMapper.selectByExample(example);
    }
}
