package com.muping.security.controller;

import com.google.common.collect.Lists;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.dal.entity.UserMenu;
import com.muping.security.service.MenuService;
import com.muping.security.service.UserMenuService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * menu控制器
 */
@RestController
@RequestMapping("/userMenu")
public class UserMenuController {

    private static final Logger logger = LoggerFactory.getLogger(UserMenuController.class);

    @Resource
    private MenuService menuService;
    @Resource
    private UserMenuService userMenuService;

    /**
     * menuId查询
     *
     * @param userId userId
     * @return 通用结果
     */
    @RequestMapping("/queryByUserId")
    public ResponseResult queryByUserId(Long userId) {
        logger.info("userId={}", userId);
        List<UserMenu> userMenuList = userMenuService.queryByUserId(userId);
        List<Long> menuIdList = buildMenuIdList(userMenuList);
        return ResponseResult.generateResponseResult(menuIdList);
    }

    /**
     * 构造menuIdList
     *
     * @param userMenuList userMenuList
     * @return menuIdList
     */
    private List<Long> buildMenuIdList(List<UserMenu> userMenuList) {
        List<Long> list = Lists.newArrayList();
        if (CollectionUtils.isEmpty(userMenuList)) {
            return list;
        }
        list = userMenuList.stream().map(UserMenu::getMenuId).distinct().collect(Collectors.toList());
        return list;
    }

}
