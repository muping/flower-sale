package com.muping.security.controller;

import com.muping.security.common.exception.ResponseResult;
import com.muping.security.service.AuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author muping
 * @date 19:16 2018/5/2
 * 权限认证
 */
@RestController
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Resource
    private AuthService authService;

    /**
     * 外部应用鉴权使用
     */
    @RequestMapping("/api/auth")
    public ResponseResult auth(String token, String url) {
        return authService.auth(token, url);
    }
}
