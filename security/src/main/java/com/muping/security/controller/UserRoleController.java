package com.muping.security.controller;

import com.google.common.collect.Lists;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.dal.entity.Role;
import com.muping.security.dal.entity.UserRole;
import com.muping.security.service.RoleService;
import com.muping.security.service.UserRoleService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * rolePermission控制器
 */
@RestController
@RequestMapping("/userRole")
public class UserRoleController {

    private static final Logger logger = LoggerFactory.getLogger(UserRoleController.class);

    @Resource
    private RoleService roleService;
    @Resource
    private UserRoleService userRoleService;

    /**
     * roleId查询
     *
     * @param userId userId
     * @return 通用结果
     */
    @RequestMapping("/queryByUserId")
    public ResponseResult queryByUserId(Long userId) {
        logger.info("userId={}", userId);
        List<UserRole> userRoleList = userRoleService.queryByUserId(userId);
        List<Long> roleIdList = buildRoleIdList(userRoleList);
        List<Role> roleList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(roleIdList)) {
            roleList = roleService.queryByRoleIdList(roleIdList);
        }
        return ResponseResult.generateResponseResult(roleList);
    }

    /**
     * 构造roleIdList
     *
     * @param userRoleList userRoleList
     * @return roleIdList
     */
    private List<Long> buildRoleIdList(List<UserRole> userRoleList) {
        List<Long> list = Lists.newArrayList();
        if (CollectionUtils.isEmpty(userRoleList)) {
            return list;
        }
        list = userRoleList.stream().map(UserRole::getRoleId).distinct().collect(Collectors.toList());
        return list;
    }

}
