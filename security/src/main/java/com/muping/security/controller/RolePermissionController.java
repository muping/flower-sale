package com.muping.security.controller;

import com.google.common.collect.Lists;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.dal.entity.Permission;
import com.muping.security.dal.entity.RolePermission;
import com.muping.security.service.PermissionService;
import com.muping.security.service.RolePermissionService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * rolePermission控制器
 */
@RestController
@RequestMapping("/rolePermission")
public class RolePermissionController {

    private static final Logger logger = LoggerFactory.getLogger(RolePermissionController.class);

    @Resource
    private RolePermissionService rolePermissionService;
    @Resource
    private PermissionService permissionService;

    /**
     * roleId查询
     *
     * @param roleId roleId
     * @return 通用结果
     */
    @RequestMapping("/queryByRoleId")
    public ResponseResult queryByRoleId(Long roleId) {
        logger.info("roleId={}", roleId);
        List<RolePermission> rolePermissionList = rolePermissionService.queryByRoleId(roleId);
        List<Long> roleIdList = buildPermissionIdList(rolePermissionList);
        List<Permission> permissionList = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(roleIdList)) {
            permissionList=permissionService.queryByPermissionIdList(roleIdList);
        }
        return ResponseResult.generateResponseResult(permissionList);
    }

    /**
     * 构造roleIdList
     *
     * @param rolePermissionList rolePermissionList
     * @return roleIdList
     */
    private List<Long> buildPermissionIdList(List<RolePermission> rolePermissionList) {
        List<Long> list = Lists.newArrayList();
        if (CollectionUtils.isEmpty(rolePermissionList)) {
            return list;
        }
        list = rolePermissionList.stream().map(RolePermission::getPermissionId).distinct().collect(Collectors.toList());
        return list;
    }

}
