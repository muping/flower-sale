package com.muping.security.controller;

import com.google.common.collect.Lists;
import com.muping.security.common.AppContext;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.common.page.Page;
import com.muping.security.common.utils.JsonUtils;
import com.muping.security.dal.entity.User;
import com.muping.security.data.condition.QueryUserListCondition;
import com.muping.security.dto.input.InputUserDto;
import com.muping.security.dto.input.QueryUserListInputDto;
import com.muping.security.dto.output.QueryUserListOutputDto;
import com.muping.security.service.UserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * user控制器
 */
@RestController
@RequestMapping("/user")
@SuppressWarnings("all")
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserService userService;

    /**
     * 查询所有user
     *
     * @return 所有user
     */
    @RequestMapping("/queryUserList")
    public ResponseResult queryUserList(QueryUserListInputDto dto) {
        logger.info("inputDto={}", JsonUtils.toString(dto));
        QueryUserListCondition condition = dto.convertToCondition();
        List<User> users = userService.queryUserList(condition);
        List<QueryUserListOutputDto> outputDtos = Lists.newArrayList();
        if (CollectionUtils.isNotEmpty(users)) {
            for (User user : users) {
                QueryUserListOutputDto outputDto = new QueryUserListOutputDto(user);
                outputDtos.add(outputDto);
            }
        }
        Page page = Page.generatePageResult(outputDtos);
        return ResponseResult.generateResponseResult(page);
    }

    /**
     * 查询登陆用户
     *
     * @return 所有user
     */
    @RequestMapping("/info")
    public ResponseResult queryUserInfo() {
        return ResponseResult.generateResponseResult(AppContext.getUser());
    }

    /**
     * id查询
     *
     * @return user
     */
    @RequestMapping("/queryById")
    public ResponseResult queryById(Long userId) {
        logger.info("userId={}", userId);
        return ResponseResult.generateResponseResult(userService.queryById(userId));
    }

    /**
     * 删除user
     *
     * @return 删除信息
     */
    @RequestMapping("/delete")
    public ResponseResult delete(Long userId) {
        logger.info("userId={}", userId);
        userService.delete(userId);
        return ResponseResult.generateSuccessMsgResponseResult("删除成功");
    }

    /**
     * 添加或者更新user
     *
     * @return 信息
     */
    @RequestMapping("/input")
    public ResponseResult input(InputUserDto dto) {
        logger.info("dto={}", JsonUtils.toString(dto));
        if (dto.getId() != null) {
            //update
            userService.update(dto);
            return ResponseResult.generateSuccessMsgResponseResult("更新成功");
        } else {
            //add
            userService.add(dto);
            return ResponseResult.generateSuccessMsgResponseResult("添加成功");
        }
    }
}
