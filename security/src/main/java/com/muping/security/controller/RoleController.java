package com.muping.security.controller;

import com.muping.security.common.exception.ResponseResult;
import com.muping.security.common.page.Page;
import com.muping.security.common.utils.JsonUtils;
import com.muping.security.dal.entity.Role;
import com.muping.security.data.condition.QueryRoleListCondition;
import com.muping.security.dto.input.InputRoleInputDto;
import com.muping.security.dto.input.QueryRoleListInputDto;
import com.muping.security.service.RolePermissionService;
import com.muping.security.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * role控制器
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

    @Resource
    private RoleService roleService;
    @Resource
    private RolePermissionService rolePermissionService;

    /**
     * 查询分页role
     *
     * @return 所有role
     */
    @RequestMapping("/queryRoleList")
    public ResponseResult queryRoleList(QueryRoleListInputDto dto) {
        logger.info("inputDto={}", JsonUtils.toString(dto));
        QueryRoleListCondition condition = dto.convertToCondition();
        List<Role> roles = roleService.queryRoleList(condition);
        Page page = Page.generatePageResult(roles);
        return ResponseResult.generateResponseResult(page);
    }

    /**
     * 查询所有role
     *
     * @return 所有role
     */
    @RequestMapping("/queryAll")
    public ResponseResult queryAll() {
        List<Role> roles = roleService.queryAll();
        return ResponseResult.generateResponseResult(roles);
    }

    /**
     * id查询
     *
     * @return role
     */
    @RequestMapping("/queryById")
    public ResponseResult queryById(Long roleId) {
        logger.info("roleId={}", roleId);
        Role role = roleService.queryById(roleId);
        return ResponseResult.generateResponseResult(role);
    }

    /**
     * 删除role
     *
     * @return 删除信息
     */
    @RequestMapping("/delete")
    public ResponseResult delete(Long roleId) {
        logger.info("roleId={}", roleId);
        roleService.delete(roleId);
        return ResponseResult.generateSuccessMsgResponseResult("删除成功");
    }

    /**
     * 添加或者更新role
     *
     * @return 信息
     */
    @RequestMapping("/input")
    public ResponseResult input(InputRoleInputDto dto) {
        logger.info("dto={}", JsonUtils.toString(dto));
        if (dto.getId() != null) {
            //update
            roleService.update(dto);
            return ResponseResult.generateSuccessMsgResponseResult("更新成功");
        } else {
            //add
            roleService.add(dto);
            return ResponseResult.generateSuccessMsgResponseResult("添加成功");
        }
    }
}
