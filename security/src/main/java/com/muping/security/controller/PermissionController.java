package com.muping.security.controller;

import com.muping.security.common.exception.ResponseResult;
import com.muping.security.common.page.Page;
import com.muping.security.common.utils.JsonUtils;
import com.muping.security.dal.entity.Permission;
import com.muping.security.data.condition.QueryPermissionListCondition;
import com.muping.security.dto.input.QueryPermissionListInputDto;
import com.muping.security.service.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 权限控制器
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);

    @Resource
    private PermissionService permissionService;

    /**
     * 查询分页权限
     *
     * @return 分页权限
     */
    @RequestMapping("/queryPermissionList")
    public ResponseResult queryPermissionList(QueryPermissionListInputDto dto) {
        logger.info("inputDto={}", JsonUtils.toString(dto));
        QueryPermissionListCondition condition = dto.convertToCondition();
        List<Permission> permissions = permissionService.queryPermissionList(condition);
        Page page = Page.generatePageResult(permissions);
        return ResponseResult.generateResponseResult(page);
    }

    /**
     * 查询所用权限
     *
     * @return 所用权限
     */
    @RequestMapping("/queryAll")
    public ResponseResult queryAll() {
        List<Permission> permissions = permissionService.queryAll();
        return ResponseResult.generateResponseResult(permissions);
    }

    /**
     * id查询
     *
     * @return 权限
     */
    @RequestMapping("/queryById")
    public ResponseResult queryById(Long permissionId) {
        logger.info("permissionId={}", permissionId);
        return ResponseResult.generateResponseResult(permissionService.queryById(permissionId));
    }

    /**
     * 删除权限
     *
     * @return 删除信息
     */
    @RequestMapping("/delete")
    public ResponseResult delete(Long permissionId) {
        logger.info("permissionId={}", permissionId);
        permissionService.delete(permissionId);
        return ResponseResult.generateSuccessMsgResponseResult("删除成功");
    }

    /**
     * 添加或者更新权限
     *
     * @return 信息
     */
    @RequestMapping("/input")
    public ResponseResult input(Permission permission) {
        logger.info("permission={}", JsonUtils.toString(permission));
        if (permission.getId() != null) {
            //update
            permissionService.update(permission);
            return ResponseResult.generateSuccessMsgResponseResult("修改成功");
        } else {
            //add
            permissionService.add(permission);
            return ResponseResult.generateSuccessMsgResponseResult("添加成功");
        }
    }
}
