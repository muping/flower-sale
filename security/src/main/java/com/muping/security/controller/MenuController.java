package com.muping.security.controller;

import com.google.common.collect.Lists;
import com.muping.security.common.exception.BusinessException;
import com.muping.security.common.exception.ResponseResult;
import com.muping.security.common.utils.JsonUtils;
import com.muping.security.dal.entity.Menu;
import com.muping.security.data.vo.MenuTreeVo;
import com.muping.security.data.vo.SystemMenuVo;
import com.muping.security.dto.input.AddMenuInputDto;
import com.muping.security.dto.input.UpdateMenuInputDto;
import com.muping.security.dto.output.QueryByParentIdOutputDto;
import com.muping.security.service.MenuService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * menu控制器
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    private static final Logger logger = LoggerFactory.getLogger(MenuController.class);

    @Resource
    private MenuService menuService;

    /**
     * 添加菜单
     *
     * @param dto menu
     * @return ResponseResult
     */
    @RequestMapping("/add")
    public ResponseResult addMenu(AddMenuInputDto dto) {
        logger.info(JsonUtils.toString(dto));
        menuService.insert(dto);
        return ResponseResult.generateSuccessMsgResponseResult("添加成功");
    }

    /**
     * 加载用户菜单
     *
     * @return ResponseResult
     */
    @RequestMapping("/load")
    public ResponseResult load() {
        List<SystemMenuVo> voList = menuService.load();
        return ResponseResult.generateResponseResult(voList);
    }

    /**
     * 查询整个菜单树
     *
     * @return ResponseResult
     */
    @RequestMapping("/queryMenuTree")
    public ResponseResult queryMenuTree() {
        List<MenuTreeVo> voList = menuService.queryMenuTree();
        return ResponseResult.generateResponseResult(voList);
    }

    /**
     * 删除菜单
     *
     * @param menuId 菜单id
     * @return ResponseResult
     */
    @RequestMapping("/delete")
    public ResponseResult delete(Long menuId) {
        menuService.delete(menuId);
        return ResponseResult.generateSuccessMsgResponseResult("删除成功");
    }

    /**
     * 更新菜单
     *
     * @param dto 菜单
     * @return ResponseResult
     */
    @RequestMapping("/update")
    public ResponseResult updateMenu(UpdateMenuInputDto dto) {
        menuService.update(dto);
        return ResponseResult.generateSuccessMsgResponseResult("更新成功");
    }

    /**
     * 根据父节点查询子节点
     *
     * @param parentId 父节点
     * @return 子节点集合
     */
    @RequestMapping("/queryByParentId")
    public ResponseResult queryByParentId(Long parentId) {
        List<Menu> menuList = menuService.queryByParentId(parentId);
        List<QueryByParentIdOutputDto> dtoList = Lists.newArrayList();
        for (Menu menu : menuList) {
            List<Menu> childMenuList = menuService.queryByParentId(menu.getId());
            Boolean isParent;
            isParent = CollectionUtils.isNotEmpty(childMenuList);
            QueryByParentIdOutputDto dto = new QueryByParentIdOutputDto(menu, isParent);
            dtoList.add(dto);
        }
        return ResponseResult.generateResponseResult(dtoList);
    }

    /**
     * 根据父节点查询查询子节点的个数
     *
     * @param parentId 父节点
     * @return 子节点的个数
     */
    @RequestMapping("/queryCountByParentId")
    public ResponseResult queryCountByParentId(Long parentId) {
        List<Menu> menuList = menuService.queryByParentId(parentId);
        if (CollectionUtils.isEmpty(menuList)) {
            return ResponseResult.generateSuccessMsgResponseResult("true");
        }
        throw new BusinessException(10002);
    }
}
